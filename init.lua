local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)

-- Load support for intllib.
local S, NS = dofile(modpath.."/intllib.lua")

---------------------------------------------
-- Wardrobe node adapted from wardrobe mod --
-- from prestidigitator licence WTFYPL	   --
---------------------------------------------
-- https://forum.minetest.net/viewtopic.php?f=9&t=9680
-- https://github.com/prestidigitator/minetest-mod-wardrobe
---------------------------------------------

local function showForm(player)
   local playerName = player:get_player_name();
   if not playerName or playerName == "" then return; end
   local formspec = "size[8,8.6]"
					.. "position[0.2,0.5]"
					.. skins.formspec.main(playerName)
					.. "button_exit[0,.75;2,.5;;Close]"

   minetest.show_formspec(playerName,"skins_set_wardrobe",formspec);
end

local id
if skins and skins.list then id = #skins.list end

-- formspec control from simple skin
minetest.register_on_player_receive_fields(function(player, formname, fields)

	if formname ~= "skins_set_wardrobe" then
		return
	end

	local name = player:get_player_name()

	if fields.skins then
		inventory_plus.set_inventory_formspec(player,
			skins.formspec.main(name) .. "button[0,.75;2,.5;main;Back]")
	end

	local event = minetest.explode_textlist_event(fields["skins_set"])

	if event.type == "CHG" then

		local index = event.index

		if index > id then index = id end

		skins.skins[name] = skins.list[index]

		if skins.invplus then
			inventory_plus.set_inventory_formspec(player,
				skins.formspec.main(name) .. "button[0,.75;2,.5;main;Back]")
		end

		skins.update_player_skin(player)
	end
end)


minetest.register_node(":simple_skins:wardrobe",
   {
      description = S("Wardrobe"),
      paramtype2 = "facedir",
      tiles = {
                 "wardrobe_wardrobe_topbottom.png",
                 "wardrobe_wardrobe_topbottom.png",
                 "wardrobe_wardrobe_sides.png",
                 "wardrobe_wardrobe_sides.png",
                 "wardrobe_wardrobe_sides.png",
                 "wardrobe_wardrobe_front.png"
              },

      inventory_image = "wardrobe_wardrobe_front.png",
      sounds = default.node_sound_wood_defaults(),
      groups = { choppy = 3, oddly_breakable_by_hand = 2, flammable = 3 },
	  on_rightclick = function(pos, node, player, itemstack, pointedThing)
         minetest.after(2,showForm,player);
      end
   });

minetest.register_craft(
   {
      output = "simple_skins:wardrobe",
      recipe = { { "group:wood", "group:stick", "group:wood" },
                 { "group:wood", "group:wool",  "group:wood" },
                 { "group:wood", "group:wool",  "group:wood" } }
   });
  
---------------------------------------------
